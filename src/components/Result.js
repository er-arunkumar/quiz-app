import React, { useContext } from 'react';
import { QuizContext } from '../assets/Context';
import questions from '../assets/questions.json';

export default function Result() {
    const { setUsername, score, setScore, setGameStart, username } = useContext(QuizContext);

    const restartQuiz = () => {
        setScore(0);
        setGameStart("user");
        setUsername("");
    };

    return (
        <div className="flex items-center justify-center min-h-screen bg-gradient-to-r from-purple-500 to-pink-600">
            <div className="bg-white p-8 rounded-lg shadow-lg max-w-sm w-full text-center">
                <h1 className="text-4xl font-bold mb-4">Result</h1>
                <h3 className="text-2xl mb-2">{username}</h3>
                <h1 className="text-3xl font-bold mb-4">{score}/{questions.length}</h1>
                <button onClick={restartQuiz} className="w-full bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                    Restart Quiz
                </button>
            </div>
        </div>
    );
}
