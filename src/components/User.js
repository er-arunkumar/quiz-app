import React, { useContext, useEffect, useRef } from 'react';
import { QuizContext } from '../assets/Context';

export default function User() {
    const { setGameStart, setUsername } = useContext(QuizContext);
    const inputRef = useRef(null);

    useEffect(() => {
        inputRef.current.focus();
    }, []);

    const startQuiz = () => {
      const username = inputRef.current.value.trim();
        if (username) {
            setUsername(username);
            setGameStart("playing");
        } else {
            alert('Username cannot be empty');
        }
    };

    return (
        <div className="flex items-center justify-center min-h-screen bg-gradient-to-r from-blue-500 to-purple-600">
         
            <div className="bg-white p-8 rounded-lg shadow-lg max-w-sm w-full">
            <h1 className='text-2xl font-medium text-gray-700 mb-3 text-center'>Welcome to Quiz App</h1>
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">Username</label>
                <input ref={inputRef} type="text" placeholder="Enter your username" className="mb-4 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"/>
                <button onClick={startQuiz} className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Start Quiz</button>
                
            </div>
            
        </div>
    );
}
