import React, { useContext, useState } from 'react';
import questions from '../assets/questions.json';
import { QuizContext } from '../assets/Context';

export default function Quiz() {
    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [optionChosen, setOptionChosen] = useState("");
    const { score, setScore, setGameStart } = useContext(QuizContext);

    const nextQuestion = () => {
        if (questions[currentQuestion].answer === optionChosen) {
            setScore(score + 1);
        }
        setCurrentQuestion(currentQuestion + 1);
        setOptionChosen("");
    };

    const finishQuiz = () => {
        if (questions[currentQuestion].answer === optionChosen) {
            setScore(score + 1);
        }
        setGameStart("finished");
    };

    return (
        <div className="flex items-center justify-center min-h-screen bg-gradient-to-r from-green-500 to-teal-600">
            <div className="bg-white p-8 rounded-lg shadow-lg max-w-xl w-full">
                <h3 className="text-2xl font-bold mb-4">{questions[currentQuestion].prompt}</h3>
                <div className="grid grid-cols-1 gap-4">
                    {["A", "B", "C", "D"].map((option) => (
                        <button
                            key={option}
                            onClick={() => setOptionChosen(`option${option}`)}
                            className={`py-2 px-4 rounded-lg shadow ${optionChosen === `option${option}` ? 'bg-blue-500 text-white' : 'bg-gray-200 hover:bg-gray-300'}`}
                        >
                            {questions[currentQuestion][`option${option}`]}
                        </button>
                    ))}
                </div>
                {currentQuestion === questions.length - 1 ? (
                    <button onClick={finishQuiz} className="mt-4 w-full bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">
                        Finish Quiz
                    </button>
                ) : (
                    <button onClick={nextQuestion} className="mt-4 w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                        Next Question
                    </button>
                )}
            </div>
        </div>
    );
}
