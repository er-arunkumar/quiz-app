import { useState } from 'react';
import { QuizContext } from './assets/Context';
import Quiz from './components/Quiz';
import Result from './components/Result';
import User from './components/User';



function App() {
  const [gameStart,setGameStart] = useState("user");
  const [username,setUsername] = useState("");
  const [score,setScore] = useState(0);
  console.log(username);

  return (
    <>
    <QuizContext.Provider value={{gameStart,setGameStart,username,setUsername,score,setScore,}}> 
    {gameStart === "user"  && <User/>}
     {gameStart === "playing"  && <Quiz/>} 
     {gameStart === "finished"  && <Result/>}
     
    </QuizContext.Provider>
    
    </>
  );
}

export default App;
