# Quiz App

### Live Demo
[Quiz App Live Demo](https://quiz-app-mauve-seven.vercel.app/) 

### Features
- **Responsive Design**: Built with TailwindCSS for a modern, responsive UI.
- **User Interaction**: Users can input their name and start the quiz.
- **Question Handling**: The app dynamically handles quiz questions and evaluates user answers.
- **Result Display**: The app displays the user's score at the end of the quiz.

### Technologies Used
- **React**: A JavaScript library for building user interfaces.
- **TailwindCSS**: A utility-first CSS framework for rapid UI development.
- **Vercel**: A cloud platform for static sites and Serverless Functions.



### Getting Started

#### Prerequisites
- Node.js (v12 or later)
- npm (v6 or later) or yarn

#### Installation
1. Clone the repository:
    ```sh
    git clone https://gitlab.com/er-arunkumar/quiz-app.git
    cd quiz-app
    ```

2. Install dependencies:
    ```sh
    npm install
    # or
    yarn install
    ```

#### Running Locally
To run the project locally:
```sh
npm start
# or
yarn start
```
#### Author: 

- Arunkumar